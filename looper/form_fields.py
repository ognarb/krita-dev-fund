import typing

from django import forms
from django.forms import NumberInput

from .money import Money


class MoneyInput(NumberInput):
    def __init__(self, attrs: typing.Optional[dict] = None) -> None:
        super().__init__({
            'placeholder': '123.45',
            **(attrs or {}),
        })


class MoneyFormField(forms.FloatField):
    """Juggle int (cents), float (units of currency), and Money."""

    def clean(self, value: str) -> typing.Optional[int]:
        """Convert from float-as-string to integer number of cents."""
        as_float = super().clean(value)
        if as_float is None:
            return None
        return int(round(as_float * 100))

    def prepare_value(self, value: typing.Union[None, int, str, Money]) -> typing.Optional[str]:
        """Turn a possible input value into a string for display in the widget.

        String input is interpreted as floats (e.g. units of currency).
        Int input is interpreted as cents.
        Money is money.
        """
        if value is None:
            return None
        if isinstance(value, str):
            if value == '':
                return None
            value = int(round(float(value) * 100))
        if isinstance(value, int):
            value = Money('', value)
        assert isinstance(value, Money)
        return value.decimals_string

    def has_changed(self,
                    initial: typing.Union[None, int, str, Money],
                    data: typing.Union[None, int, str, Money]) -> bool:
        return self.prepare_value(initial) != self.prepare_value(data)


class GatewayRadioSelect(forms.RadioSelect):
    option_template_name = 'looper/forms/widgets/gateway_radio_option.html'

    def create_option(self, name, value, label, selected, index, subindex=None, attrs=None) -> dict:
        gateway = self.choices.queryset.get(name=value)
        return {
            **super().create_option(name, value, label, selected, index, subindex=subindex,
                                    attrs=attrs),
            'form_description': gateway.form_description,
        }


class GatewayChoiceField(forms.ModelChoiceField):
    """ModelChoiceField for choosing a payment gateway.

    Stores the gateway name in the field value. This allows JavaScript to know
    which gateway is selected without having to resort to hard-coding primary
    keys.
    """

    def __init__(self, **kwargs) -> None:
        from . import models
        kwargs.setdefault('empty_label', None)
        kwargs.setdefault('label', 'Payment method')
        kwargs.setdefault('queryset', models.Gateway.objects.all())
        kwargs.setdefault('to_field_name', 'name')
        kwargs.setdefault('widget', GatewayRadioSelect())
        super().__init__(**kwargs)

    def label_from_instance(self, obj) -> str:
        return obj.frontend_name
