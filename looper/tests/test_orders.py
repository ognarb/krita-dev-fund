from unittest import mock

import django.utils.timezone

from . import AbstractLooperTestCase


class OrdersTest(AbstractLooperTestCase):
    def test_create_from_subscription(self):
        subscription = self.create_subscription()
        order = subscription.generate_order()
        self.assertEqual('created', order.status)
        self.assertEqual('Blender Development Fund Membership / Gold', order.name)

    def test_subscription_activation(self):
        subscription = self.create_subscription()
        order = subscription.generate_order()

        # Even without a transaction in the correct state,
        # setting an order to 'paid' should activate the
        # connected subscription.
        order.status = 'paid'

        now = django.utils.timezone.now()
        with mock.patch('django.utils.timezone.now') as mock_now:
            mock_now.return_value = now
            order.save(update_fields={'status'})

        subscription.refresh_from_db()
        order.refresh_from_db()
        self.assertEqual('paid', order.status)
        self.assertEqual(now, order.paid_at)
        self.assertEqual('active', subscription.status)
        self.assertEqual('active', subscription.status)

    def test_subscription_already_active(self):
        subscription = self.create_subscription()
        subscription.status = 'active'
        subscription.save()

        order = subscription.generate_order()
        order.status = 'paid'
        order.save()

        subscription.refresh_from_db()
        order.refresh_from_db()
        self.assertEqual('paid', order.status)
        self.assertEqual('active', subscription.status)

    def test_subscription_active_order_cancelled(self):
        subscription = self.create_subscription()
        subscription.status = 'active'
        subscription.save()

        order = subscription.generate_order()
        order.status = 'cancelled'
        order.save()

        subscription.refresh_from_db()
        order.refresh_from_db()
        self.assertEqual('cancelled', order.status)
        self.assertEqual('active', subscription.status,
                         'Cancelling an order should not influence an already-active subscription')

    def test_subscription_direct_active_order_fulfilled(self):
        # This way the subscription is marked as 'active' while
        # the order is still in 'created' state. The order skipping
        # the 'paid' state shouldn't matter to the subscription.
        subscription = self.create_subscription()
        subscription.status = 'active'
        subscription.save()

        order = subscription.generate_order()
        order.status = 'fulfilled'
        order.save()

        subscription.refresh_from_db()
        order.refresh_from_db()
        self.assertEqual('fulfilled', order.status)
        self.assertEqual('active', subscription.status)

    def test_subscription_active_order_fulfilled(self):
        # Contrary to the test above, this test lets the order go through
        # the 'paid' status, and lets that activate the subscription.
        # Fulfilling the order still shouldn't change the Subscription status.
        subscription = self.create_subscription()
        order = subscription.generate_order()
        order.status = 'paid'
        order.save()

        subscription.refresh_from_db()
        self.assertEqual('active', subscription.status)

        order.status = 'fulfilled'
        order.save()

        subscription.refresh_from_db()
        order.refresh_from_db()
        self.assertEqual('fulfilled', order.status)
        self.assertEqual('active', subscription.status)

    def test_subscription_inactive_order_fulfilled(self):
        subscription = self.create_subscription()
        order = subscription.generate_order()
        order.status = 'paid'
        order.save()

        # Mimick a race condition between a subscription going to 'on-hold'
        # and after that the order going from 'paid' to 'fulfilled'.
        # This should not change the subscription status.
        subscription.status = 'on-hold'
        subscription.save()
        order.status = 'fulfilled'
        order.save()

        subscription.refresh_from_db()
        order.refresh_from_db()
        self.assertEqual('fulfilled', order.status)
        self.assertEqual('on-hold', subscription.status)
