from unittest import mock

from django.contrib.admin.models import LogEntry

import looper.exceptions
from . import AbstractLooperTestCase
from .. import admin_log


class TransactionsTestCase(AbstractLooperTestCase):
    def test_transaction_create_from_subscription(self) -> None:
        subscription = self.create_subscription()

        # We used to generate the initial order automatically, but no longer.
        order = subscription.latest_order()
        self.assertIsNone(order)

        order = subscription.generate_order()
        self.assertIsNone(order.latest_transaction())

    @mock.patch('looper.gateways.BraintreeGateway.transact_sale')
    def test_charge(self, mock_transact_sale) -> None:
        subscription = self.create_subscription()
        order = subscription.generate_order()
        trans = order.generate_transaction()
        mock_transact_sale.assert_not_called()

        mock_transact_sale.return_value = 'mocked-transaction-id'
        self.assertTrue(trans.charge(customer_ip_address='fe80::5ad5:4eaf:feb0:4747'))

        trans.refresh_from_db()
        self.assertEqual('mocked-transaction-id', trans.transaction_id)
        self.assertEqual(trans.ip_address, 'fe80::5ad5:4eaf:feb0:4747')

        # The order should be marked as 'paid'.
        order.refresh_from_db()
        self.assertEqual('paid', order.status)

        # The subscription should have been activated.
        subscription.refresh_from_db()
        self.assertEqual('active', subscription.status)

        # A LogEntry should have been created for this success.
        entries_q = admin_log.entries_for(trans)
        self.assertEqual(1, len(entries_q))
        entry: LogEntry = entries_q.first()
        self.assertIn('charge', entry.change_message.lower())
        self.assertIn('success', entry.change_message.lower())

    @mock.patch('looper.gateways.BraintreeGateway.transact_sale')
    def test_charge_gateway_error(self, mock_transact_sale) -> None:
        subscription = self.create_subscription()
        order = subscription.generate_order()
        trans = order.generate_transaction()
        mock_transact_sale.assert_not_called()

        mock_transact_sale.side_effect = looper.exceptions.GatewayError(
            'mocked error', errors=['äuẅ'])
        self.assertFalse(trans.charge(customer_ip_address='fe80::5ad5:4eaf:feb0:4747'))

        # The transaction should be marked as failed.
        trans.refresh_from_db()
        self.assertEqual('failed', trans.status)
        self.assertEqual('', trans.transaction_id)
        self.assertEqual(trans.ip_address, 'fe80::5ad5:4eaf:feb0:4747')

        # The order and subscription shouldn't be influenced.
        order.refresh_from_db()
        subscription.refresh_from_db()
        self.assertEqual('created', order.status)
        self.assertEqual('on-hold', subscription.status)

        # A LogEntry should have been created for this success.
        entries_q = admin_log.entries_for(trans)
        self.assertEqual(1, len(entries_q))
        entry: LogEntry = entries_q.first()
        self.assertIn('charge', entry.change_message.lower())
        self.assertIn('failed', entry.change_message.lower())

        self.assertEqual('mocked error: äuẅ', trans.failure_message)
        self.assertFalse(trans.paid)

    @mock.patch('looper.gateways.BraintreeGateway.transact_sale')
    def test_charge_bad_status(self, mock_transact_sale) -> None:
        subscription = self.create_subscription()
        order = subscription.generate_order()
        trans = order.generate_transaction()
        trans.status = 'failed'
        trans.save()

        mock_transact_sale.return_value = 'mocked-transaction-id'
        with self.assertRaises(looper.exceptions.IncorrectStatusError):
            trans.charge(customer_ip_address='fe80::5ad5:4eaf:feb0:4747')

        mock_transact_sale.assert_not_called()

        # This is a programming error, so it shouldn't create a log event.
        entries_q = admin_log.entries_for(trans)
        self.assertEqual(0, len(entries_q))

        self.assertEqual('', trans.failure_message)
        self.assertFalse(trans.paid)
