import typing
from unittest import mock

from django.urls import reverse

from looper.views.settings import PaymentMethodReplaceView
from ..middleware import PREFERRED_CURRENCY_SESSION_KEY
from .. import forms, gateways
from . import AbstractLooperTestCase


class PaymentMethodChangeTest(AbstractLooperTestCase):
    fixtures = AbstractLooperTestCase.fixtures + ['mock-gateway']

    def setUp(self):
        super().setUp()
        self.client.force_login(self.user)

    def _happy_change(
            self, subs,
            to_gateway: typing.Type[gateways.AbstractPaymentGateway] = gateways.MockableGateway,
            expect_new_payment_method=True,
    ):
        orig_pm = subs.payment_method
        self.assertIsNotNone(orig_pm)
        paymeth_count_before = self.user.paymentmethod_set.count()

        url = reverse('looper:payment_method_change', kwargs={'subscription_id': subs.id})

        with mock.patch('looper.gateways.MockableGateway.generate_client_token') as mock_gct:
            mock_gct.return_value = 'mock-client-auth-token'
            resp = self.client.get(url)
        self.assertEqual(200, resp.status_code, resp.content.decode())

        form = forms.ChangePaymentMethodForm({
            'payment_method_nonce': 'new-pm-nonce',
            'gateway': to_gateway.gateway_name,
            'next_url_after_done': '/je-moeder',
        })
        form.full_clean()
        gw_name = f'{to_gateway.__module__}.{to_gateway.__name__}'
        with mock.patch(f'{gw_name}.customer_create') as mock_cc, \
                mock.patch(f'{gw_name}.payment_method_create') as mock_pmc:
            mock_cc.return_value = 'mock-customer-id'

            mock_paymeth = mock.Mock(spec=gateways.PaymentMethodInfo)
            mock_paymeth.token = 'new-mock-payment-token'
            mock_paymeth.recognisable_name.return_value = 'new-mock-recognisable-name'
            mock_paymeth.type_for_database.return_value = 'pa'
            mock_pmc.return_value = mock_paymeth

            resp = self.client.post(url, data=form.cleaned_data)

        self.assertEqual(302, resp.status_code, resp.content.decode())
        self.assertEqual('/je-moeder', resp['Location'])

        # A new payment method could have been added.
        self.assertEqual(paymeth_count_before + expect_new_payment_method,
                         self.user.paymentmethod_set.count())

        new_pm = self.user.paymentmethod_set.exclude(pk=orig_pm.pk).first()
        return new_pm, orig_pm

    def test_happy_change_subs_order_paid(self):
        subs = self.create_active_subscription()
        order = subs.latest_order()
        self.assertEqual('paid', order.status)

        new_pm, orig_pm = self._happy_change(subs)

        subs.refresh_from_db()
        order.refresh_from_db()
        self.assertEqual(orig_pm.pk, subs.latest_order().payment_method.pk)
        self.assertEqual(new_pm.pk, subs.payment_method.pk)

    def test_happy_change_subs_and_order(self):
        subs = self.create_on_hold_subscription()
        order = subs.latest_order()

        self.assertEqual('created', order.status)
        new_pm, orig_pm = self._happy_change(subs)

        subs.refresh_from_db()
        order.refresh_from_db()
        self.assertEqual(new_pm.pk, order.payment_method.pk)
        self.assertEqual(new_pm.pk, subs.payment_method.pk)
        self.assertEqual('created', order.status)

    def test_mock_to_bank_to_mock(self):
        subs = self.create_on_hold_subscription()
        order = subs.latest_order()

        self.assertEqual('created', order.status)
        new_pm, orig_pm = self._happy_change(subs, gateways.BankGateway)

        subs.refresh_from_db()
        order.refresh_from_db()
        self.assertEqual(new_pm.pk, order.payment_method.pk)
        self.assertEqual(new_pm.pk, subs.payment_method.pk)
        self.assertEqual('created', order.status)

        # Should have switched to manual, because automatic isn't supported for banks.
        self.assertEqual('manual', subs.collection_method)
        self.assertEqual('manual', order.collection_method)

        # Moving back should also move to automatic
        self._happy_change(subs, gateways.MockableGateway)
        subs.refresh_from_db()
        order.refresh_from_db()
        self.assertEqual('automatic', subs.collection_method)
        self.assertEqual('automatic', order.collection_method)

    def test_mock_to_bank_to_bank(self):
        subs = self.create_on_hold_subscription()
        order = subs.latest_order()

        self.assertEqual('created', order.status)
        new_pm1, orig_pm1 = self._happy_change(subs, gateways.BankGateway)
        self._happy_change(subs, gateways.MockableGateway)
        new_pm2, orig_pm2 = self._happy_change(subs, gateways.BankGateway,
                                               expect_new_payment_method=False)

        self.assertEqual(new_pm1, new_pm2,
                         'Switching from bank to bank should keep the payment method')

    def test_different_currency(self):
        self.client.get('/')  # Get any URL to start a session.
        self.assertEqual('USD', self.client.session[PREFERRED_CURRENCY_SESSION_KEY])

        subs = self.create_active_subscription()
        self.assertEqual('EUR', subs.currency)

        url = reverse('looper:payment_method_change', kwargs={'subscription_id': subs.id})

        passed_currency = ''

        def generate_client_token(self, for_currency: str,
                                  gateway_customer_id: typing.Optional[str] = None) -> str:
            nonlocal passed_currency
            passed_currency = for_currency
            return 'mock-client-auth-token'

        with mock.patch('looper.gateways.MockableGateway.generate_client_token',
                        new=generate_client_token) as mock_gct:
            resp = self.client.get(url)
        self.assertEqual(200, resp.status_code, resp.content.decode())
        self.assertEqual('EUR', passed_currency, 'Expected the subscription currency to be used')


class PaymentMethodReplaceTest(AbstractLooperTestCase):
    fixtures = AbstractLooperTestCase.fixtures + ['mock-gateway']

    def setUp(self):
        super().setUp()
        self.client.force_login(self.user)

    def test_happy_change_subs_and_order(self):
        subs_on_hold = self.create_on_hold_subscription()
        order_subs_on_hold = subs_on_hold.latest_order()
        self.assertEqual('on-hold', subs_on_hold.status)
        self.assertEqual('created', order_subs_on_hold.status)

        subs_active = self.create_active_subscription()
        order_subs_active = subs_active.latest_order()
        self.assertEqual('active', subs_active.status)
        self.assertEqual('paid', order_subs_active.status)

        subs_cancelled = self.create_on_hold_subscription()
        subs_cancelled.cancel_subscription()
        order_subs_cancelled = subs_cancelled.latest_order()
        self.assertEqual('cancelled', subs_cancelled.status)
        self.assertEqual('cancelled', order_subs_cancelled.status)

        self.assertEqual(1, self.user.paymentmethod_set.count())
        orig_pm = self.user.paymentmethod_set.first()
        url = reverse('looper:payment_method_replace', kwargs={'pk': orig_pm.id})

        # The session should have one preferred currency, whereas the payment method
        # was used to pay with a different currency.
        self.client.get('/')  # Get any URL to start a session.
        self.assertEqual('USD', self.client.session[PREFERRED_CURRENCY_SESSION_KEY])
        self.assertEqual('EUR', subs_active.currency)

        passed_currency = ''

        def generate_client_token(self, for_currency: str,
                                  gateway_customer_id: typing.Optional[str] = None) -> str:
            nonlocal passed_currency
            passed_currency = for_currency
            return 'mock-client-auth-token'

        with mock.patch('looper.gateways.MockableGateway.generate_client_token',
                        new=generate_client_token) as mock_gct:
            resp = self.client.get(url)
        self.assertEqual(200, resp.status_code, resp.content.decode())
        self.assertEqual('EUR', passed_currency, 'Expected the subscription currency to be used')

        form = PaymentMethodReplaceView.form_class({
            'payment_method_nonce': 'new-pm-nonce',
            'gateway': 'mock',
        })
        form.full_clean()
        with mock.patch('looper.gateways.MockableGateway.customer_create') as mock_cc, \
                mock.patch('looper.gateways.MockableGateway.payment_method_create') as mock_pmc:
            mock_cc.return_value = 'mock-customer-id'

            mock_paymeth = mock.Mock(spec=gateways.PaymentMethodInfo)
            mock_paymeth.token = 'new-mock-payment-token'
            mock_paymeth.recognisable_name.return_value = 'new-mock-recognisable-name'
            mock_paymeth.type_for_database.return_value = 'pa'
            mock_pmc.return_value = mock_paymeth

            resp = self.client.post(url, data=form.cleaned_data)

        self.assertEqual(302, resp.status_code, resp.content.decode())
        self.assertEqual(reverse('looper:payment_methods'), resp['Location'])

        # A new payment method should have been added.
        self.assertEqual(2, self.user.paymentmethod_set.count())
        new_pm = self.user.paymentmethod_set.exclude(pk=orig_pm.pk).first()

        subs_active.refresh_from_db()
        subs_on_hold.refresh_from_db()
        subs_cancelled.refresh_from_db()
        order_subs_on_hold.refresh_from_db()
        order_subs_active.refresh_from_db()
        order_subs_cancelled.refresh_from_db()

        # It should be used for the still-payable stuff, but not for already-paid/cancelled stuff.
        self.assertEqual(new_pm.pk, subs_active.payment_method_id,
                         f'Expected subs of status {subs_active.status!r} to change PM')
        self.assertEqual(new_pm.pk, subs_on_hold.payment_method_id,
                         f'Expected subs of status {subs_on_hold.status!r} to change PM')
        self.assertEqual(new_pm.pk, order_subs_on_hold.payment_method_id,
                         f'Expected order of status {order_subs_on_hold.status!r} to change PM')

        self.assertEqual(orig_pm.pk, subs_cancelled.payment_method_id)
        self.assertEqual(orig_pm.pk, order_subs_active.payment_method_id)
        self.assertEqual(orig_pm.pk, order_subs_cancelled.payment_method_id)
