from . import middleware, models


def preferred_currency_name(request) -> str:
    session_currency = request.session.get(middleware.PREFERRED_CURRENCY_SESSION_KEY)
    return session_currency or models.DEFAULT_CURRENCY


def preferred_currency(request) -> dict:
    """
    Makes preferred currency available as template variable `preferred_currency`.
    """

    return {'preferred_currency': preferred_currency_name(request)}
