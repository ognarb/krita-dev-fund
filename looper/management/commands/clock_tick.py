import logging

from django.core.management.base import BaseCommand

import looper.clock

log = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'Run the Looper subscription clock.'

    def add_arguments(self, parser):
        parser.add_argument('--dry-run', '-n',
                            action='store_true',
                            default=False,
                            help='Only log what will happen, do not execute for real.')

    def handle(self, *args, dry_run: bool, verbosity: int, **options):
        if dry_run:
            verbosity = max(3, verbosity)
            cls = looper.clock.SimulatingClock
        else:
            cls = looper.clock.Clock

        levels = {
            0: logging.ERROR,
            1: logging.WARNING,
            2: logging.INFO,
            3: logging.DEBUG,
        }
        level = levels.get(verbosity, logging.DEBUG)
        logging.getLogger('looper').setLevel(level)
        logging.disable(level - 1)

        clock = cls()
        clock.tick()
