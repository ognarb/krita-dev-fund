from django.apps import AppConfig


class LooperConfig(AppConfig):
    name = 'looper'

    def ready(self):
        import looper.signals  # noqa
