import logging

from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth import get_user_model
from django.http import HttpResponse, Http404
from django.shortcuts import redirect, get_object_or_404, render
from django.views import View
from django.views.generic import FormView, DetailView, ListView

from .. import exceptions, forms, middleware, models, utils

log = logging.getLogger(__name__)
User = get_user_model()


class ChooseDefaultPlanVariationView(View):
    """Chose the appropriate variation for this plan, then forward the user to checkout."""
    log = log.getChild('ChooseDefaultPlanVariationView')

    def get(self, request, plan_id: int):
        plan = get_object_or_404(models.Plan, pk=plan_id)

        # Determine the proper plan for the user's currency.
        prefcur: str = request.session.get(middleware.PREFERRED_CURRENCY_SESSION_KEY,
                                           models.DEFAULT_CURRENCY)

        default_variation = plan.variation_for_currency(prefcur)
        if default_variation is None and prefcur != models.DEFAULT_CURRENCY:
            log.warning('Default variation in %s for Plan %i not found' % (prefcur, plan_id))
            # Check if the a variation is available in the default currency
            default_variation = plan.variation_for_currency(models.DEFAULT_CURRENCY)
            if default_variation is None:
                log.error('No variation for for Plan %i found' % plan_id)
                raise Http404('This Plan does not have variations at the moment.')

        return redirect('looper:checkout',
                        plan_id=plan_id,
                        plan_variation_id=default_variation.id)


class ChoosePlanVariationView(ListView):
    template_name = 'checkout/choose_plan_variation.html'

    def get(self, request, *args, **kwargs):
        self.plan = get_object_or_404(models.Plan, pk=kwargs['plan_id'])
        self.current_pv = get_object_or_404(
            self.plan.variations.active(),
            pk=kwargs['current_plan_variation_id'])
        return super().get(request, *args, **kwargs)

    def get_queryset(self):
        # Determine the current currency.
        currency: str = self.request.session.get(middleware.PREFERRED_CURRENCY_SESSION_KEY,
                                                 models.DEFAULT_CURRENCY)
        # Fetch only active variations in the current currency.
        return self.plan.variations.active().filter(currency=currency)

    def get_context_data(self, *, object_list=None, **kwargs) -> dict:
        return {
            **super().get_context_data(object_list=object_list, **kwargs),
            'plan': self.plan,
            'current_plan_variation': self.current_pv,
        }


class AbstractPaymentView(LoginRequiredMixin, FormView):
    """Superclass for Braintree payment screens."""
    log = log.getChild('AbstractPaymentView')

    gateway: models.Gateway
    customer: models.Customer
    session_key_prefix = 'PAYMENT_GATEWAY_CLIENT_TOKEN_'

    def dispatch(self, request, *args, **kwargs):
        if not getattr(self, 'gateway', None):
            # Only set self.gateway if it hasn't been set already. This allows
            # subclasses to override the default gateway in their dispatch()
            # method before calling super().dispatch().
            self.gateway = models.Gateway.default()
        self.user = self.request.user
        if self.user.is_authenticated:
            self.customer = self.user.customer
        return super().dispatch(request, *args, **kwargs)

    def get_initial(self) -> dict:
        return {
            'gateway': self.gateway.name,
        }

    def gateway_from_form(self, form) -> models.Gateway:
        self.gateway = form.cleaned_data['gateway']
        return self.gateway

    def get_context_data(self, **kwargs) -> dict:
        currency = self.get_currency()
        ctx = {
            **super().get_context_data(**kwargs),
            'client_token': self.get_client_token(currency),
        }
        return ctx

    def get_currency(self) -> str:
        """Returns the currency for the current page.

        Defaults to the user's preferred currency, but can be overridden
        in a subclass.
        """
        return self.request.session.get(middleware.PREFERRED_CURRENCY_SESSION_KEY,
                                        models.DEFAULT_CURRENCY)

    def client_token_session_key(self, for_currency: str) -> str:
        return f'{self.session_key_prefix}{self.gateway.pk}_{for_currency}'

    def get_client_token(self, for_currency: str) -> str:
        """Generate a Braintree client token, caching it in the session.

        This is hard-coded to Braintree instead of using the current payment
        gateway, because the user can switch between gateways in the web
        interface and thus we have to be able to show the Braintree drop-in
        UI at any moment.
        """

        sesskey = self.client_token_session_key(for_currency)
        try:
            return self.request.session[sesskey]
        except KeyError:
            pass

        # TODO(Sybren): handle Gateway errors.
        braintree_gw = models.Gateway.objects.get(name__in={'braintree', 'mock'})
        gateway_customer_id = self.customer.gateway_customer_id(braintree_gw)
        client_token = braintree_gw.provider.generate_client_token(
            for_currency, gateway_customer_id)

        self.request.session[sesskey] = client_token
        return client_token

    def erase_client_token(self) -> None:
        """Erase the client token from the session."""

        to_erase = [key for key in self.request.session.keys()
                    if key.startswith(self.session_key_prefix)]
        for sesskey in to_erase:
            del self.request.session[sesskey]


class CheckoutView(AbstractPaymentView):
    """Perform the checkout procedure for a subscription."""

    template_name = 'checkout/checkout.html'
    anonymous_template_name = 'checkout/checkout_anonymous.html'
    form_class = forms.CheckoutForm

    log = log.getChild('CheckoutView')

    plan: models.Plan
    plan_variation: models.PlanVariation

    def dispatch(self, request, *args, **kwargs):
        # Get the prerequisites for handling any request in this view.
        self.plan = get_object_or_404(models.Plan, pk=kwargs['plan_id'])
        self.plan_variation = get_object_or_404(
            self.plan.variations.active(),
            pk=kwargs['plan_variation_id'])

        # Show a simpler version of the view when anonymous.
        if request.user.is_anonymous and request.method == 'GET':
            return render(request, self.anonymous_template_name, {
                'plan': self.plan,
                'plan_variation': self.plan_variation,
            })

        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs) -> dict:
        ctx = {
            **super().get_context_data(**kwargs),
            'plan': self.plan,
            'plan_variation': self.plan_variation,
        }
        return ctx

    def get_form_kwargs(self) -> dict:
        return {
            **super().get_form_kwargs(),
            'instance': self.customer.billing_address,
        }

    def get_initial(self) -> dict:
        return {
            **super().get_initial(),
            'full_name': self.customer.full_name,
        }

    def form_valid(self, form: forms.CheckoutForm) -> HttpResponse:
        gateway = self.gateway_from_form(form)
        gateway_provider = gateway.provider

        # Update billing address if it changed.
        if form.has_changed():
            form.save()

        nonce = form.cleaned_data['payment_method_nonce']
        try:
            payment_method = self.customer.payment_method_add(nonce, gateway)
        except exceptions.GatewayError as e:
            self.log.info('Error adding payment method: %s', e.errors)
            form.add_error('', f'Error from the payment gateway: {e.with_errors()} '
                               'Please refresh the page and try again.')
            return self.form_invalid(form)

        # Create Subscription using the default settings (will be 'on-hold')
        # TODO Make this a method of PlanVariation, like create_subscription
        subs_pk = form.cleaned_data.get('subscription_pk')
        if subs_pk:
            subscription: models.Subscription = get_object_or_404(
                self.user.subscription_set, pk=subs_pk)
            self.log.debug('Reusing subscription pk=%r for checkout', subscription.pk)
        else:
            collection_method = self.plan_variation.collection_method
            if collection_method not in gateway_provider.supported_collection_methods:
                # TODO(Sybren): when automatic is not supported, we need to switch to manual.
                # However, we may want to notify the user about this beforehand.
                # We also may want to choose a collection method more explicitly than just
                # picking a supported one randomly. In practice this is not so random, though,
                # as we only have two supported collection methods (automatic and manual),
                # so if one is not supported this always picks the other one.
                supported = set(gateway_provider.supported_collection_methods)
                collection_method = supported.pop()

            subscription = models.Subscription.objects.create(
                plan=self.plan,
                user=self.user,
                payment_method=payment_method,
                price=self.plan_variation.price,
                currency=self.plan_variation.currency,
                interval_unit=self.plan_variation.interval_unit,
                interval_length=self.plan_variation.interval_length,
                collection_method=collection_method,
            )
            self.log.debug('Created new subscription pk=%r for checkout', subscription.pk)

        # Either get the existing order, or create a new one. Once the order is
        # created we perform a Charge on a new transaction.
        order_pk = form.cleaned_data.get('order_pk')
        if order_pk:
            order: models.Order = get_object_or_404(subscription.order_set, pk=order_pk)
            self.log.debug('Reusing order pk=%r for checkout', order.pk)
        else:
            latest_order = subscription.latest_order()
            if latest_order and latest_order.status == 'created':
                # There already was some order created on this subscription, so try
                # and pay for that.
                order = latest_order
            else:
                # This is the expected situation: a new subscription won't have any order
                # on it yet, so we have to create it.
                order = subscription.generate_order()

            self.log.debug('Using auto-created order pk=%r for checkout', order.pk)

        # Only follow through with a charge if it's actually supported.
        if not gateway_provider.supports_transactions:
            self.log.info('Not creating transaction for order pk=%r because gateway %r does '
                          'not support it', order.pk, gateway.name)
            self.erase_client_token()
            return redirect('looper:transactionless_checkout_done',
                            pk=order.pk, gateway_name=gateway.name)

        trans = order.generate_transaction()

        customer_ip_address = utils.get_client_ip(self.request)
        if not trans.charge(customer_ip_address=customer_ip_address):
            form.add_error('', trans.failure_message)
            form.data = {
                **form.data.dict(),  # form.data is a QueryDict, which is immutable.
                'subscription_pk': subscription.pk,
                'order_pk': order.pk,
            }
            return self.form_invalid(form)

        # The transaction, being marked as 'paid', will automatically
        # activate the subscription.
        self.erase_client_token()
        return redirect('looper:checkout_done', transaction_id=trans.pk)


class DoneView(LoginRequiredMixin, DetailView):
    template_name = 'checkout/checkout_done.html'

    def get_object(self, queryset=None) -> models.Order:
        transaction_id: int = self.kwargs['transaction_id']
        transactions = self.request.user.transaction_set
        transaction: models.Transaction = get_object_or_404(transactions, pk=transaction_id)
        return transaction.order


class TransactionlessCheckoutDoneView(LoginRequiredMixin, DetailView):
    template_name = 'checkout/checkout_done_transactionless.html'

    def get_queryset(self):
        return self.request.user.order_set

    def get_context_data(self, **kwargs) -> dict:
        gateway = get_object_or_404(models.Gateway, name=self.kwargs['gateway_name'])
        return {
            **super().get_context_data(**kwargs),
            'gateway': gateway,
        }


class CheckoutExistingOrderView(AbstractPaymentView):
    """Perform the checkout procedure for an existing order."""

    template_name = 'checkout/checkout_existing_order.html'
    form_class = forms.CheckoutForm

    log = log.getChild('CheckoutExistingOrderView')

    order: models.Order
    plan: models.Plan

    def dispatch(self, request, *args, **kwargs):
        # Get the prerequisites for handling any request in this view.
        self.order = get_object_or_404(models.Order, pk=kwargs['order_id'])
        if self.order.user_id != request.user.id:
            return render(request, 'checkout/not_your_order.html', status=403)
        self.plan = self.order.subscription.plan
        return super().dispatch(request, *args, **kwargs)

    def get_currency(self) -> str:
        return self.order.currency

    def get_context_data(self, **kwargs) -> dict:
        ctx = {
            **super().get_context_data(**kwargs),
            'order': self.order,
            'subscription': self.order.subscription,
            'plan': self.plan,
        }
        return ctx

    def get_form_kwargs(self) -> dict:
        return {
            **super().get_form_kwargs(),
            'instance': self.customer.billing_address,
        }

    def get_initial(self) -> dict:
        return {
            **super().get_initial(),
            'full_name': self.customer.full_name,
        }

    def form_valid(self, form: forms.CheckoutForm) -> HttpResponse:
        gateway = self.gateway_from_form(form)

        # Update billing address if it changed.
        if form.has_changed():
            form.save()

        nonce = form.cleaned_data['payment_method_nonce']
        try:
            payment_method = self.customer.payment_method_add(nonce, gateway)
        except exceptions.GatewayError as e:
            self.log.info('Error adding payment method: %s', e.errors)
            form.add_error('', 'Cannot use a payment nonce more than once. '
                               'Please refresh the page and try again')
            return self.form_invalid(form)

        # Switch to this new payment method.
        subs = self.order.subscription
        if subs.payment_method is None or subs.payment_method.pk != payment_method.pk:
            self.log.info('Switching subscription pk=%d to payment method pk=%d',
                          subs.pk, payment_method.pk)
            subs.payment_method = payment_method
            subs.save(update_fields={'payment_method'})

        order = self.order
        if order.payment_method is None or order.payment_method.pk != payment_method.pk:
            self.log.info('Switching order pk=%d to payment method pk=%d',
                          order.pk, payment_method.pk)
            order.payment_method = payment_method
            order.save(update_fields={'payment_method'})

        # Charge a new transaction.
        trans = order.generate_transaction()
        customer_ip_address = utils.get_client_ip(self.request)
        if not trans.charge(customer_ip_address=customer_ip_address):
            form.add_error('', trans.failure_message)
            return self.form_invalid(form)

        # The transaction, being marked as 'paid', will automatically
        # activate the subscription.
        self.erase_client_token()
        return redirect('looper:checkout_done', transaction_id=trans.pk)
