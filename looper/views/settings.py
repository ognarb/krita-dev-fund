import logging

from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponse, HttpResponseForbidden, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse_lazy
from django.views.generic import UpdateView, ListView, FormView, DeleteView

import looper.views.checkout
from .. import exceptions, forms, models

log = logging.getLogger(__name__)


class PersonalInfoView(LoginRequiredMixin, UpdateView):
    template_name = 'settings/personal_info.html'
    form_class = forms.CustomerForm
    model = models.Customer
    success_url = reverse_lazy('looper:settings_personal_info')

    def get_object(self, queryset=None):
        return self.request.user.customer


class BillingAddressView(LoginRequiredMixin, UpdateView):
    template_name = 'settings/billing_address.html'
    form_class = forms.AddressForm
    model = models.Address
    success_url = reverse_lazy('looper:settings_billing_info')

    def get_object(self, queryset=None):
        return self.request.user.customer.billing_address


class PaymentMethodsView(LoginRequiredMixin, ListView):
    template_name = 'settings/payment_methods.html'
    model = models.PaymentMethod

    def get_queryset(self):
        superset = super().get_queryset()
        return superset.filter(user=self.request.user,
                               is_deleted=False)


class PaymentMethodDeleteView(LoginRequiredMixin, DeleteView):
    template_name = 'settings/payment_method_delete.html'
    model = models.PaymentMethod
    success_url = reverse_lazy('looper:payment_methods')

    log = log.getChild('PaymentMethodDeleteView')

    def delete(self, request, *args, **kwargs):
        payment_method = self.get_object()
        if self.request.user.id != payment_method.user.id:
            self.log.warning("User %d tried to delete someone else's payment method %d",
                             self.request.user.pk, payment_method.pk)

            return HttpResponseForbidden(content='No access to this payment method')

        self.log.info('Deleting payment method pk=%d', payment_method.pk)
        return super().delete(request, *args, **kwargs)


class PaymentMethodChangeView(looper.views.checkout.AbstractPaymentView):
    """Use the Braintree drop-in UI to switch a subscription's payment method."""

    template_name = 'settings/payment_method_change.html'
    form_class = forms.ChangePaymentMethodForm
    success_url = '/settings/'

    log = log.getChild('PaymentMethodChangeView')

    subscription: models.Subscription

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            self.subscription = get_object_or_404(request.user.subscription_set,
                                                  pk=kwargs['subscription_id'])
        return super().dispatch(request, *args, **kwargs)

    def get_currency(self) -> str:
        return self.subscription.currency

    def get_context_data(self, **kwargs) -> dict:
        ctx = {
            **super().get_context_data(**kwargs),
            'subscription': self.subscription,
            'current_payment_method': self.subscription.payment_method,
        }
        return ctx

    def get_initial(self) -> dict:
        return {
            **super().get_initial(),
            'next_url_after_done': self.request.META.get('HTTP_REFERER', '/')
        }

    def form_valid(self, form: forms.ChangePaymentMethodForm) -> HttpResponse:
        gateway = self.gateway_from_form(form)

        nonce = form.cleaned_data['payment_method_nonce']
        try:
            payment_method = self.customer.payment_method_add(nonce, gateway)
        except exceptions.GatewayError as e:
            self.log.info('Error adding payment method: %s', e.errors)
            form.add_error('', e.with_errors())
            return self.form_invalid(form)

        if self.subscription.payment_method_id == payment_method.id:
            self.log.debug('User tried to replace a payment method with itself')
            form.add_error('', f'Please select a different payment method with which to replace '
                               f'{self.subscription.payment_method.recognisable_name}.')
            return self.form_invalid(form)

        # Switch to this new payment method.
        subs = self.subscription
        if subs.payment_method is None or subs.payment_method.pk != payment_method.pk:
            subs.attach_log_entry(f'User {self.user} changed their payment method to '
                                  f'{payment_method.recognisable_name}')
            subs.switch_payment_method(payment_method)

        order = subs.latest_order()
        if order is None:
            # If there is no order, we don't have to switch it either.
            self.log.warning('Switching payment method on subscription %r without order', subs.pk)
        elif order.status in {'soft-failed', 'created'} and (
                order.payment_method is None or order.payment_method.pk != payment_method.pk):
            # Also switch the order to the new payment method.
            order.attach_log_entry(f'User {self.user} changed their payment method to '
                                   f'{payment_method.recognisable_name}')
            order.switch_payment_method(payment_method)

        self.erase_client_token()
        return HttpResponseRedirect(form.cleaned_data['next_url_after_done'] or self.success_url)


class PaymentMethodReplaceView(looper.views.checkout.AbstractPaymentView):
    """Use the Braintree drop-in UI to replace a payment method."""

    template_name = 'settings/payment_method_replace.html'
    form_class = forms.PaymentMethodReplaceForm
    success_url = reverse_lazy('looper:payment_methods')

    log = log.getChild('PaymentMethodReplaceView')

    paymeth: models.PaymentMethod
    currency: str

    def dispatch(self, request, *args, **kwargs):
        self.paymeth = get_object_or_404(request.user.paymentmethod_set, pk=kwargs['pk'])
        self.currency = self.determine_currency()
        if not self.currency:
            return render(request, 'settings/payment_method_not_used.html')

        return super().dispatch(request, *args, **kwargs)

    def determine_currency(self) -> str:
        """Determine the currency for which this payment method was used.

        It could be more than one, but we just pick one from the last
        transaction, order, or subscription.
        """
        trans = self.paymeth.transaction_set.last()
        if trans is not None:
            return trans.currency

        order = self.paymeth.order_set.last()
        if order is not None:
            return order.currency

        subs = self.paymeth.subscription_set.last()
        if subs is not None:
            return subs.currency

        return ''

    def get_currency(self) -> str:
        return self.currency

    def get_context_data(self, **kwargs) -> dict:
        ctx = {
            **super().get_context_data(**kwargs),
            'paymeth': self.paymeth,
            'currency': self.currency,
        }
        return ctx

    def form_valid(self, form: forms.CheckoutForm) -> HttpResponse:
        gateway = self.gateway_from_form(form)

        nonce = form.cleaned_data['payment_method_nonce']
        try:
            new_paymeth = self.customer.payment_method_add(nonce, gateway)
        except exceptions.GatewayError as e:
            self.log.info('Error adding payment method: %s', e.errors)
            form.add_error('', e.with_errors())
            return self.form_invalid(form)

        if self.paymeth.pk == new_paymeth.pk:
            self.log.debug('User tried to replace a payment method with itself')
            form.add_error('', f'Please select a different payment method with which to replace '
                               f'{self.paymeth.recognisable_name}.')
            return self.form_invalid(form)

        # Switch to this new payment method.
        for subs in self.paymeth.subscription_set.payable():
            subs.switch_payment_method(new_paymeth)
        for order in self.paymeth.order_set.payable():
            order.switch_payment_method(new_paymeth)

        self.erase_client_token()
        return super().form_valid(form)  # redirect to self.success_url
