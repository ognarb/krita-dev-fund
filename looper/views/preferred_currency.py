import logging

from django.conf import settings
from django.http import JsonResponse
from django.views import View

from ..middleware import PREFERRED_CURRENCY_SESSION_KEY

log = logging.getLogger(__name__)


class CurrencySelectorView(View):
    """JSON view, POST to set default currency, GET to obtain."""

    log = log.getChild('CurrencySelectorView')

    def get(self, request, *args, **kwargs) -> JsonResponse:
        preferred_currency = request.session.get(PREFERRED_CURRENCY_SESSION_KEY, None)
        return JsonResponse({'preferred_currency': preferred_currency})

    def post(self, request, *args, **kwargs) -> JsonResponse:
        # TODO(Sybren): store this with the user if logged in.
        try:
            prefcur = request.POST['preferred_currency']
        except KeyError:
            return JsonResponse({'_message': 'missing field "preferred_currency"'}, status=400)

        if prefcur not in settings.SUPPORTED_CURRENCIES:
            supported = ', '.join(sorted(settings.SUPPORTED_CURRENCIES))
            response = {'_message': f'unsupported currency {prefcur!r}, choose from {supported}'}
            return JsonResponse(response, status=400)

        self.log.debug('Setting preferred currency to %r', prefcur)
        request.session['PREFERRED_CURRENCY'] = prefcur
        return self.get(request, *args, **kwargs)
