import io
import logging
import pathlib
import re
import typing

from django.conf import settings
from django.template.response import TemplateResponse
from django.contrib.staticfiles import finders

import xhtml2pdf.pisa as pisa

log = logging.getLogger(__name__)


class PDFResponse(TemplateResponse):
    md5_re = re.compile(r'\.[0-9a-f]{12}(\.[^.]+)$')

    def __init__(self, request, template, context=None,
                 content_type: typing.Optional[str] = None,
                 status=None, charset=None, using=None) -> None:
        if content_type is None:
            content_type = 'application/pdf'
        super().__init__(request, template, context, content_type, status, charset, using)

    @classmethod
    def link_callback(cls, uri: str, rel) -> str:
        """
        Convert HTML URIs to absolute system paths so xhtml2pdf can access those
        resources
        """
        # use short variable names
        sUrl = settings.STATIC_URL  # Typically /static/
        mUrl = settings.MEDIA_URL  # Typically /static/media/
        mRoot = settings.MEDIA_ROOT  # Typically /home/userX/project_static/media/

        # convert URIs to absolute system paths
        try:
            if uri.startswith(mUrl):
                path = mRoot / uri.replace(mUrl, "")
            elif uri.startswith(sUrl):
                without_static = uri.replace(sUrl, '')
                found = finders.find(without_static)
                if found is None:
                    # Undo the cache-busting with the MD5 sum and try to find the file then.
                    without_md5 = cls.md5_re.sub(r'\1', without_static)
                    found = finders.find(without_md5)
                path = pathlib.Path(found)
            else:
                return uri  # handle absolute uri (ie: http://some.tld/foo.png)
        except Exception:
            log.exception('Unexpected exception finding %r', uri)
            return uri

        # make sure that file exists
        if not path.is_file():
            log.error('Unable to find PDF-referenced file %s', path)
            return uri
        return str(path)

    @property
    def rendered_content(self) -> bytes:
        """Return the freshly rendered content for the template and context
        described by the TemplateResponse.

        This *does not* set the final content of the response. To set the
        response content, you must either call render(), or set the
        content explicitly using the value of this property.
        """
        template = self.resolve_template(self.template_name)
        context = self.resolve_context(self.context_data)
        content = template.render(context, self._request)

        response = io.BytesIO()
        pdf = pisa.pisaDocument(io.BytesIO(content.encode('utf8')), response,
                                link_callback=self.link_callback)
        if pdf.err:
            log.error('Error rendering %r with context %r to PDF: %s',
                      template, context, pdf.err)
            return b'Error Rendering PDF'

        return response.getvalue()
