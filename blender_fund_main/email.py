import logging
import typing

from django.conf import settings
from django.contrib.sites.shortcuts import get_current_site
import django.core.mail
from django.dispatch import receiver
from django.template import loader
from django.urls import reverse

import looper.signals
import looper.models
from . import models, signals

log = logging.getLogger(__name__)


def absolute_url(viewname: str,
                 args: typing.Optional[tuple] = None,
                 kwargs: typing.Optional[dict] = None) -> str:
    """Same as django.urls.reverse() but then as absolute URL.

    For simplicity this assumes HTTPS is used.
    """
    from urllib.parse import urljoin

    domain = get_current_site(None).domain
    relative_url = reverse(viewname, args=args, kwargs=kwargs)
    return urljoin(f'https://{domain}/', relative_url)


def is_noreply(email: str) -> bool:
    """Return True if the email address is a no-reply address."""
    return email.startswith('noreply@') or email.startswith('no-reply@')


@receiver(signals.membership_activated)
@receiver(signals.membership_cancelled)
def send_mail_membership_status_changed(sender: models.Membership, **kwargs):
    """Send out an email notifying about the activated membership."""
    my_log = log.getChild('send_mail_membership_status_changed')

    email = sender.user.email
    if is_noreply(email):
        my_log.debug('Not sending membership-changed notification to no-reply address %s', email)
        return

    my_log.debug('Sending membership-changed notification to %s', email)

    email_body_html, email_body_txt, subject = _construct_membership_mail(membership=sender)

    try:
        django.core.mail.send_mail(
            subject,
            message=email_body_txt,
            html_message=email_body_html,
            from_email=None,  # just use the configured default From-address.
            recipient_list=[email],
            fail_silently=False,
        )
    except OSError:
        my_log.exception('Error sending membership-changed notification to %s',
                         email)
    else:
        my_log.info('Sent membership-changed notification to %s', email)


def _construct_membership_mail(membership: models.Membership) -> typing.Tuple[str, str, str]:
    """Construct a mail about a membership.

    :return: tuple (html, text, subject)
    """
    # Construct the link to Blender ID dynamically, to prevent hard-coding it in the email.
    url = absolute_url('settings_membership_edit', kwargs={'membership_id': membership.pk})

    if membership.status == 'active':
        verb = 'activated'
    else:
        verb = 'deactivated'

    context = {
        'user': membership.user,
        'customer': membership.user.customer,
        'membership': membership,
        'link': url,
        'verb': verb,
    }

    subject: str = loader.render_to_string(
        f'blender_fund_main/emails/membership_{verb}_subject.txt', context)
    context['subject'] = subject.strip()

    email_body_html = loader.render_to_string(
        f'blender_fund_main/emails/membership_{verb}.html', context)
    email_body_txt = loader.render_to_string(
        f'blender_fund_main/emails/membership_{verb}.txt', context)

    return email_body_html, email_body_txt, context['subject']


@receiver(signals.membership_created_needs_payment)
def send_mail_membership_bank_info(sender: models.Membership, **kwargs):
    """Send out an email notifying about the required bank payment."""
    my_log = log.getChild('send_mail_membership_bank_info')

    email = sender.user.email
    if is_noreply(email):
        my_log.debug('Not sending membership-bank-info notification to no-reply address %s', email)
        return
    my_log.debug('Sending membership-bank-info notification to %s', email)

    membership = sender
    url = absolute_url('settings_membership_edit', kwargs={'membership_id': membership.pk})

    context = {
        'user': membership.user,
        'customer': membership.user.customer,
        'membership': membership,
        'subscription': membership.subscription,
        'link': url,
    }

    subject: str = loader.render_to_string(
        f'blender_fund_main/emails/membership_bankinfo_subject.txt', context).strip()
    context['subject'] = subject

    email_body_html = loader.render_to_string(
        f'blender_fund_main/emails/membership_bankinfo.html', context)
    email_body_txt = loader.render_to_string(
        f'blender_fund_main/emails/membership_bankinfo.txt', context)

    try:
        django.core.mail.send_mail(
            subject,
            message=email_body_txt,
            html_message=email_body_html,
            from_email=None,  # just use the configured default From-address.
            recipient_list=[email],
            fail_silently=False,
        )
    except OSError:
        my_log.exception('Error sending membership-bank-info notification to %s',
                         email)
    else:
        my_log.info('Sent membership-bank-info notification to %s', email)


@receiver(looper.signals.automatic_payment_succesful)
@receiver(looper.signals.automatic_payment_soft_failed)
@receiver(looper.signals.automatic_payment_failed)
def automatic_payment_performed(sender: looper.models.Order,
                                transaction: looper.models.Transaction,
                                **kwargs):
    """Send out an email notifying about the soft-failed payment."""
    my_log = log.getChild('automatic_payment_soft_failed')

    user = sender.user
    customer = user.customer
    billing_email = customer.billing_email or user.email
    my_log.debug('Sending %r notification to %s', sender.status, billing_email)

    try:
        membership = sender.subscription.membership
    except models.Membership.DoesNotExist:
        my_log.warning('Subscription %r has no membership, not sending email '
                       'about automatic payment of order %r', sender.subscription_id, sender.pk)
        return

    membership_url = absolute_url('settings_membership_edit',
                                  kwargs={'membership_id': membership.pk})
    pay_url = absolute_url('looper:checkout_existing_order', kwargs={'order_id': sender.pk})
    receipt_url = absolute_url('settings_receipt', kwargs={'order_id': sender.pk})

    context = {
        'customer': customer,
        'email': billing_email,
        'order': sender,
        'membership': membership,
        'pay_url': pay_url,
        'receipt_url': receipt_url,
        'membership_url': membership_url,
        'failure_message': transaction.failure_message,
        'payment_method': transaction.payment_method.recognisable_name,
        'maximum_collection_attemps': settings.LOOPER_CLOCK_MAX_AUTO_ATTEMPTS,
    }

    try:
        subject: str = loader.render_to_string(
            f'blender_fund_main/emails/payment_{sender.status}_subject.txt', context).strip()
        context['subject'] = subject

        email_body_html = loader.render_to_string(
            f'blender_fund_main/emails/payment_{sender.status}.html', context)
        email_body_txt = loader.render_to_string(
            f'blender_fund_main/emails/payment_{sender.status}.txt', context)
    except Exception as ex:
        # Template rendering errors shouldn't interfere with the Looper clock, so
        # catch all errors here.
        my_log.exception('Error rendering templates to send %r notification to %s: %s',
                         sender.status, billing_email, ex)
        return

    try:
        django.core.mail.send_mail(
            subject,
            message=email_body_txt,
            html_message=email_body_html,
            from_email=None,  # just use the configured default From-address.
            recipient_list=[billing_email],
            fail_silently=False,
        )
    except OSError:
        my_log.exception('Error sending %r notification to %s',
                         sender.status, billing_email)
    else:
        my_log.info('Sent %r notification to %s', sender.status, billing_email)


@receiver(looper.signals.managed_subscription_notification)
def managed_subscription_notification(sender: looper.models.Subscription,
                                      **kwargs):
    """Send out an email notifying a manager about an expiring managed subscription."""
    my_log = log.getChild('managed_subscription_notification')
    email = settings.LOOPER_MANAGER_MAIL
    my_log.debug('Notifying %s about managed subscription %r passing its next_payment date',
                 email, sender.pk)

    user = sender.user
    membership = sender.membership
    subs_admin_url = absolute_url('admin:looper_subscription_change',
                                  kwargs={'object_id': sender.id})
    memb_admin_url = absolute_url('admin:blender_fund_main_membership_change',
                                  kwargs={'object_id': membership.id})

    context = {
        'user': user,
        'subscription': sender,
        'membership': membership,
        'subs_admin_url': subs_admin_url,
        'memb_admin_url': memb_admin_url,
    }

    try:
        subject: str = loader.render_to_string(
            f'blender_fund_main/emails/managed_memb_notif_subject.txt', context).strip()
        context['subject'] = subject

        email_body_html = loader.render_to_string(
            f'blender_fund_main/emails/managed_memb_notif.html', context)
        email_body_txt = loader.render_to_string(
            f'blender_fund_main/emails/managed_memb_notif.txt', context)
    except Exception as ex:
        # Template rendering errors shouldn't interfere with the Looper clock, so
        # catch all errors here.
        my_log.exception('Error rendering templates to send notification about managed '
                         'membership %r to %s: %s', membership.pk, email, ex)
        return

    try:
        django.core.mail.send_mail(
            subject,
            message=email_body_txt,
            html_message=email_body_html,
            from_email=None,  # just use the configured default From-address.
            recipient_list=[email],
            fail_silently=False,
        )
    except OSError as ex:
        my_log.exception('Error sending notification mail about managed '
                         'membership %r to %s: %s', membership.pk, email, ex)
    else:
        my_log.info('Notified %s about managed subscription %r passing its next_payment date',
                    email, sender.pk)
