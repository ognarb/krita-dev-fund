Dear {{ customer.full_name|default:user.email }},

Thank you for joining the Blender Development Fund. You chose to pay
for your {{ membership.level.name }} membership by bank transfer, which
means that we will be waiting for you to perform this transfer.

When paying, please mention the following:

    Blender Fund Membership subs-{{ subscription.id }}

Please send your payment of {{ subscription.price.with_currency_symbol }} to:

    Stichting Blender Foundation
    Buikslotermeerplein 161
    1025 ET Amsterdam, the Netherlands

    Bank: ING Bank
    Bijlmerdreef 109
    1102 BW Amsterdam, the Netherlands

    BIC/Swift code: INGB NL 2A
    IBAN: NL45 INGB 0009356121 (for Euro countries)

    Tax number NL 8111.66.223

You can always go to {{ link }} to view and update your membership.

Kind regards,

Blender Foundation
