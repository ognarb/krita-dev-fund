import typing
import random
from collections import defaultdict

from django.conf import settings
import django.db.models

from wagtail.core.models import Page
from wagtail.core.fields import RichTextField
from wagtail.admin.edit_handlers import FieldPanel
from wagtail.images.edit_handlers import ImageChooserPanel

import looper.context_processors
import looper.middleware
import looper.models
from looper.money import Money
import looper.money

from . import models

# Amount of money made for each membership level category.
MoneyPerCategory = typing.Mapping[str, Money]


class HomePage(Page):
    body = RichTextField(blank=True)
    tagline = django.db.models.TextField(blank=True)
    description_text = RichTextField(blank=True)

    header_image = django.db.models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=django.db.models.SET_NULL,
        related_name='+'
    )

    social_media_image = django.db.models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=django.db.models.SET_NULL,
        related_name='+'
    )

    content_panels = Page.content_panels + [
        FieldPanel('body', classname="full"),
        FieldPanel('tagline', classname="full"),
        ImageChooserPanel('header_image'),
    ]

    promote_panels = Page.promote_panels + [
        ImageChooserPanel('social_media_image'),
    ]

    subpage_types = ['blender_fund_main.SimplePage']

    template = 'blender_fund/landing.html'

    @staticmethod
    def get_total_income_in_euro() -> MoneyPerCategory:
        """Compute the total income of the Blender Foundation.

        :return: The total income, on a yearly/12 basis, converted to Euros,
            per membership level category.
        """

        # If there is no subscription, we don't know how much money
        # we're getting, so we can safely ignore those.
        active_mems = models.Membership.objects \
            .active() \
            .exclude(subscription__isnull=True) \
            .select_related('subscription', 'level')

        # Get the monthly subscription prices per membership level category.
        prices_per_cat: typing.DefaultDict[str, typing.List[Money]] = defaultdict(list)
        for mem in active_mems:
            cat: str = mem.level.category
            prices_per_cat[cat].append(mem.subscription.monthly_rounded_price)

        # Sum the prices per category per currency.
        total_per_currency: typing.Dict[str, typing.Mapping[str, Money]] = {
            cat: looper.money.sum_per_currency(prices)
            for cat, prices in prices_per_cat.items()
        }

        # Convert all partial sums to euros.
        total_in_eur = {}
        zero = Money('EUR', 0)
        for cat_name, cat_label in models.MembershipLevel.CATEGORIES:
            try:
                partial_sums = total_per_currency[cat_name]
            except KeyError:
                total_in_eur[cat_name] = zero
                continue

            total_in_eur[cat_name] = looper.money.sum_to_euros(partial_sums.values())

        return total_in_eur

    @classmethod
    def total_income_in_currency(cls, currency) -> MoneyPerCategory:
        """Convert the total Foundation income to the given currency."""

        total_in_eur_per_cat = cls.get_total_income_in_euro()
        return {
            cat: looper.money.convert_currency(total_in_eur, to_currency=currency)
            for cat, total_in_eur in total_in_eur_per_cat.items()
        }

    def thermo_settings(self, currency: str) -> dict:
        """Context data for the thermometer."""

        fund_income_target_pref = looper.money.convert_currency(
            settings.FUND_INCOME_TARGET, to_currency=currency)

        total_income = self.total_income_in_currency(currency)
        thermo_percentages = {}
        for cat, income_for_cat in total_income.items():
            ratio = income_for_cat / fund_income_target_pref
            assert isinstance(ratio, float)

            percentage = int(100. * ratio)
            thermo_percentages[cat] = percentage

        # Convert from cents to kilo-currency.
        thermo_high = fund_income_target_pref.cents // 100_000
        thermo_mid = thermo_high // 2

        return {
            'total_income': total_income,
            'summed_income': sum(total_income.values(), Money(currency, 0)),
            'thermo_high': thermo_high,
            'thermo_mid': thermo_mid,
            'currency_symbol': fund_income_target_pref.currency_symbol,
            'thermo_percentages': thermo_percentages,
            'thermo_total_percentage': sum(thermo_percentages.values()),
        }

    def get_context(self, request, *args, **kwargs):
        query = models.Membership.objects \
            .visible() \
            .select_related('level') \
            .all()
        memberships = defaultdict(list)
        for result in query:
            result.limit_fields()
            memberships[result.level.name].append(result)
        for mems in memberships.values():
            random.shuffle(mems)

        # AnonymousUser doesn't have a 'memberships' attribute.
        if request.user.is_authenticated:
            user_memberships = request.user.memberships.active()
            user_membership_levels = {
                db_values['level__name']
                for db_values in user_memberships.values('level__name')
            }
        else:
            user_membership_levels = set()

        currency = looper.context_processors.preferred_currency_name(request)
        membership_levels = models.MembershipLevel.objects \
            .filter(plan__isnull=False) \
            .exclude(category='CORP') \
            .select_related('plan') \
            .all()
        memlev_with_planvar = [(m, m.plan.variation_for_currency(currency))
                               for m in membership_levels]

        query = models.MembershipLevel.objects \
            .values('category') \
            .annotate(django.db.models.Count('membership')) \
            .order_by('category')
        memberships_per_category = {item['category']: item['membership__count']
                                    for item in query}

        # Add extra variables and return the updated context
        return {
            **super().get_context(request),
            **self.thermo_settings(currency),
            'membership_levels': memlev_with_planvar,
            'memberships': memberships,
            'user_membership_levels': user_membership_levels,
            'memberships_per_category': memberships_per_category,
            'memberships_total_count': sum(memberships_per_category.values()),
        }


class SimplePage(Page):
    body = RichTextField(blank=True)

    content_panels = Page.content_panels + [
        FieldPanel('body', classname="full"),
    ]

    template = 'blender_fund/wagtail_page_simple.html'
