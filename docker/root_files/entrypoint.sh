#!/bin/bash

set -e
echo -n "Starting up at "
date -Iseconds

echo "Ensuring log directories are set up correctly"
mkdir -p /var/log/{uwsgi,apache2,mysql,apps}/
chown -R mysql:mysql /var/log/mysql
chgrp -R django /var/log/{uwsgi,apps}
chmod -R g+ws /var/log/{uwsgi,apps}

touch /var/log/{syslog,mail.{err,log}}
chown syslog:adm /var/log/{syslog,mail.{err,log}}
chmod 640 /var/log/{syslog,mail.{err,log}}

touch /var/log/uwsgi/blender-fund.log
chown blender-fund:django /var/log/uwsgi/blender-fund.log

mkdir -p /var/www/blender-fund/media
chown blender-fund:django /var/www/blender-fund/media
chmod 2775 /var/www/blender-fund/media

echo "Ensuring Postfix spool directory is set up correctly."
/usr/lib/postfix/configure-instance.sh

echo "Starting rsyslogd"
rsyslogd

# Start Apache first, so that it can at least serve a "we'll be right back" page.
echo "Starting Apache"
apache2ctl start

# Do this before we start Postgres so we shut that down
# even when something else doesn't want to start.
function shutdown {
    echo "Shutting down"
    set +e

    [ -e /var/run/uwsgi-blender_fund.pid ] && uwsgi --stop /etc/uwsgi/apps-enabled/blender-fund.ini
    [ -e /var/run/mysqld/mysqld.pid ] && kill $(</var/run/mysqld/mysqld.pid)

    postfix stop
    apache2ctl graceful-stop
}
trap shutdown EXIT

echo "Starting Postfix"
postfix start

# Configure and start Postgres.
if [ ! -e $MYSQL_DATA/mysql ]; then
    echo "$MYSQL_DATA does not exist, initialising database"
    . /create_db.sh
else
    echo "Starting MySQL"
    mysqld --daemonize
fi

echo "Running migrations"
cd /var/www/blender-fund
pipenv run python3 manage.py migrate

echo "Starting uWSGI"
uwsgi /etc/uwsgi/apps-enabled/blender-fund.ini

echo "Waiting for stuff"
set +e
tail -f /dev/null &
KILLPID=$!

function finish {
    echo "Finishing Docker image"
    set -x
    kill $KILLPID
}
trap finish QUIT
trap finish TERM
trap finish INT

wait

echo "Done waiting, shutting down some stuff cleanly"
