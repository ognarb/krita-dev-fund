#!/usr/bin/env bash

case "$1" in
    all|full|quick|'')
        ;;
    *)
        echo "Usage: $0 quick|full" >&2
        exit 3
        ;;
esac

# macOS does not support readlink -f, so we use greadlink instead
if [ $(uname) = 'Darwin' ]; then
    command -v greadlink 2>/dev/null 2>&1 || { echo >&2 "Install greadlink using brew."; exit 1; }
    readlink='greadlink'
else
    readlink='readlink'
fi
MY_DIR="$(dirname "$($readlink -f "$0")")"

cd "$MY_DIR/docker"
exec ./deploy.sh "$@"
