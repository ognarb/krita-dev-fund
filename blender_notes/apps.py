from django.apps import AppConfig


class NotesConfig(AppConfig):
    name = 'blender_notes'
    verbose_name = 'Blender Notes'
